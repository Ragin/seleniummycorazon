package org.ragin.test

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import java.util.concurrent.TimeUnit // for wait

object CorazonTesterKotlin {
    const val URL_START = "https://concepts-system.app.sandbox.mycorazon.de/login"
    const val XPATH_BTN_LOGIN = "//button[@type='button']"
    const val XPATH_WAREHOUSE = "//div[@id='headingWarehouse']/h5/a/span"
    const val LOGIN_PASSWORD = "admin"
    const val LOGIN_USERNAME = "admin"

    @JvmStatic
    fun main(args: Array<String>) {
        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver")
        val driver: WebDriver = ChromeDriver()
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS)

        driver[URL_START]
        driver.findElement(By.name("username")).clear()
        driver.findElement(By.name("username")).sendKeys(LOGIN_USERNAME)
        driver.findElement(By.id("tbPassword")).clear()
        driver.findElement(By.id("tbPassword")).sendKeys(LOGIN_PASSWORD)
        driver.findElement(By.xpath(XPATH_BTN_LOGIN)).click()
        println(driver.title)
        driver.findElement(By.xpath(XPATH_WAREHOUSE)).click()
        driver.findElement(By.linkText("Waren")).click()
        driver.findElement(By.linkText("Artikel")).click()
        print(driver.title)
        // time.sleep(4)
        driver.close()
        driver.quit()
    }
}