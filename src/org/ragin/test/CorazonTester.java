package org.ragin.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CorazonTester {
    static final String URL_START = "https://concepts-system.app.sandbox.mycorazon.de/login";
    static final String XPATH_BTN_LOGIN = "//button[@type='button']";
    static final String XPATH_WAREHOUSE = "//div[@id='headingWarehouse']/h5/a/span";

    static final String LOGIN_PASSWORD = "admin";
    static final String LOGIN_USERNAME = "admin";

    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver","/usr/bin/chromedriver");
        WebDriver driver = new ChromeDriver();
        driver.get(URL_START);
        driver.findElement(By.name("username")).clear();
        driver.findElement(By.name("username")).sendKeys(LOGIN_USERNAME);

        driver.findElement(By.id("tbPassword")).clear();
        driver.findElement(By.id("tbPassword")).sendKeys(LOGIN_PASSWORD);
        driver.findElement(By.xpath(XPATH_BTN_LOGIN)).click();
        System.out.println(driver.getTitle());
    }
}
